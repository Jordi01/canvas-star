var c = document.getElementById("acanvas");
var ctx = c.getContext("2d");

ctx.moveTo(100, 50);
ctx.lineTo(95, 75);
ctx.moveTo(109, 75);
ctx.lineTo(99, 50);

ctx.moveTo(109, 75);
ctx.lineTo(130, 55);
ctx.moveTo(130, 55);
ctx.lineTo(120, 90);

ctx.moveTo(120, 90);
ctx.lineTo(145, 70);
ctx.moveTo(145, 70);
ctx.lineTo(100, 120);

ctx.stroke();
